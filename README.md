### Couple analytical algorithms from the field of acoustics, chosen randomly, inspired by curiosity.

<img src="https://library.kissclipart.com/20191103/tze/kissclipart-bird-duck-mallard-ducks-geese-and-swans-water-bir-abcb7dc38402ef8e.png" width=100 height=100></src>

 Folder created by Daniel Kaczor, daniel.kac94@gmail.com 

#### It is settled, this project is going to be Huygens rule based, primitive acoustics ray-tracer.

**TODO's**:
    
* simple geometrical objects, point, circle, line, object crossing methods
* spliting cases; method for diffrent object interaction
* method for checking if line crosses microphone alike circle (if sumthin' come to mic)
* is there here even some space for digital signal processing? 
* what is real attenuation (real-world aproximation)
* coupling object with material type
