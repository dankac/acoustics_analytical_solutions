import math as mth


def generate_geo_lines(vertices): #vertiecs store like: point1->point2=line, point2->point3=line, etc...
    lines = []
    #temporary workaround - iff coordinate == 0 add small value to exlude zero division
    small_am = 1E-10
    for v in range(0,len(vertices)):
        if vertices[v][0] == 0:
            vertices[v][0] += small_am
        if vertices[v][1] == 0:
            vertices[v][1] += small_am

    for v in range(0,len(vertices)-1):

        a_b = (vertices[v][1] + vertices[v+1][1]) / (vertices[v][0] + vertices[v+1][0])
        _a_b = (vertices[v][1] - vertices[v+1][1]) / (-vertices[v][0] + vertices[v+1][0])
        b = (_a_b + a_b) / 2
        lines.append([(vertices[v][1]-b)/vertices[v][0],b])

    a_b = (vertices[len(vertices)-1][1] + vertices[0][1]) / (vertices[len(vertices)-1][0] + vertices[0][0])
    _a_b = (vertices[len(vertices)-1][1] - vertices[0][1]) / (-vertices[len(vertices)-1][0] + vertices[0][0])
    b = (_a_b + a_b) / 2
    lines.append([(vertices[0][1] - b) / vertices[0][0], b])
    return lines

def ray_crossing_sink():

    raise NotImplemented

def ray_crossing_wall():
    raise NotImplemented
#calculate point of reflection if:
    # ray line is between lines from src to vertice 1 and vertice 2 which are describing the ray_crossing_wall()
    # the range of angle with ox is within the range of angle with ox between those 2 lines

def set_of_rays_at_point(n_rays, point):
    angle = 360
    #we start at 0 angle on xy plane, dividing angle by n_rays, calculating line eq. by incremening angle with ox by fraction of 2pi
    dw = int(360/n_rays)
    lines = []
    for w in range(0,360,dw):
        a = mth.tan(w*3.14/180)
        b = point[1] - a*point[0]
        lines.append([a,b])
    return lines


def main():

    point = [0] * 2
    circle = [0] * 3
    line = [0] * 2
    obstacles = [line]

    point[0] = 1
    point[1] = 2

    set_of_rays_at_point(10,point)
    vertices = [[1,10],[10,1],[1,-10],[-10,1]]
    room_lines = generate_geo_lines(vertices)

# http://www.sherwoodtheory.org/sw2015/uploads/270/azhao_fvolpe_huygensprinciple_wavefronttracing_poster.pdf

# equation of line on planes
# y = ax + b
# angle between wo lines is the angle between direction vectors of the lines
# direction vector of line in point
# calculate SOX SOY, x=0, y=0

# generate a set of lines crossing in point for different angles between THEM apples heh

# TALKING SOME SHIEEET
# shot rays from point, ray is a simple line, origin of line is the circle center,
# then we divide circle by the angles, for each angle generate a line
# having room geometry check possible crossings, (Huyens rule) :
    #every point where spherical wave arrives is the point of new spherical wave
    #rays are sended if for example two rays met and it goes on
    #for example our n rays of circle wave meet some point, then this point split in n - 1 rays

    #Algo
    0.#pick point, define wall line equation
    1.# calculate n rays as lines
    2.  # if one ray crosses with wall at some point | if one ray crosses circle which is microphone add right wave amplitude to this wave after calculating the phase
    3.  # basically the time after this ray arrvied after the first was sended (t0), we can decrease ray ampltidue during split or creating new wave-front (wall crossing)
            # send n - 1 rays (back to 1-st task) till n > "order"
        # otherwise discard that ray




main()